package ru.t1.sukhorukova.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    public static final String NAME = "project-clear";
    public static final String DESCRIPTION = "Remove all projects.";

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        getProjectService().clear();
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
