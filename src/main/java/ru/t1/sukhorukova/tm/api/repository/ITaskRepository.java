package ru.t1.sukhorukova.tm.api.repository;

import ru.t1.sukhorukova.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAllByProjectId(String projectId);

}
