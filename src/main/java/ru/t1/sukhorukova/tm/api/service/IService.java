package ru.t1.sukhorukova.tm.api.service;

import ru.t1.sukhorukova.tm.enumerated.Sort;
import ru.t1.sukhorukova.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    M add(M model);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    List<M> findAll(Sort sort);

    void clear();

    M findOneById(String id);

    M findOneByIndex(Integer index);

    void remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

    int getSize();

    boolean existsById(String id);

}
