package ru.t1.sukhorukova.tm.api.command;

public interface ICommand {

    void execute();

    String getArgument();

    String getName();

    String getDescription();

}
