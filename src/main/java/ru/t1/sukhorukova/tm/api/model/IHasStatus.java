package ru.t1.sukhorukova.tm.api.model;

import ru.t1.sukhorukova.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
