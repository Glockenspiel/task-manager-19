package ru.t1.sukhorukova.tm.service;

import ru.t1.sukhorukova.tm.api.repository.IUserRepository;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.exception.entity.EntityNotFoundException;
import ru.t1.sukhorukova.tm.exception.field.EmailEmptyException;
import ru.t1.sukhorukova.tm.exception.field.IdEmptyException;
import ru.t1.sukhorukova.tm.exception.field.LoginEmptyException;
import ru.t1.sukhorukova.tm.exception.field.PasswordEmptyException;
import ru.t1.sukhorukova.tm.exception.user.ExistsEmailException;
import ru.t1.sukhorukova.tm.exception.user.RoleEmptyException;
import ru.t1.sukhorukova.tm.model.User;
import ru.t1.sukhorukova.tm.util.HashUtil;
import ru.t1.sukhorukova.tm.api.service.IUserService;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    public UserService(IUserRepository userRepository) {
        super(userRepository);
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw  new PasswordEmptyException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw  new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw  new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();

        final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = repository.findByLogin(login);
        if (user == null) return null;
        return user;
    }

    @Override
    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = repository.findByEmail(email);
        if (user == null) return null;
        return user;
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        if (user == null) throw new EntityNotFoundException();
        return repository.remove(user);
    }

    @Override
    public User removeByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = findByEmail(email);
        if (user == null) throw new EntityNotFoundException();
        return repository.remove(user);
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = findOneById(id);
        if (user == null) throw new EntityNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(
            final String id,
            final String firstName,
            final String lastName,
            final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = findOneById(id);
        if (user == null) throw new EntityNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public Boolean isLoginExist(final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.isLoginExist(login);
    }

    @Override
    public Boolean isEmailExist(final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.isEmailExist(email);
    }

}
